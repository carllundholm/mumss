# Gives a rating between 0 and 1, based on how good the view is from the house

from mumss import *

# Meshes
mesh_air = Mesh("../../data/mesh_air.xml.gz")
mesh_house = Mesh("../../data/mesh_house.xml.gz")
mesh_house.translate(Point(0.496,0.297,0.04)) #The house close to the camera
mesh_house1 = Mesh("../../data/mesh_house.xml.gz")
mesh_house1.translate(Point(0.65,0.45,0.01))
mesh_house2 = Mesh("../../data/mesh_house.xml.gz")
mesh_house2.translate(Point(0.55,0.50,0.01))
mesh_house3 = Mesh("../../data/mesh_house.xml.gz")
mesh_house3.translate(Point(0.4,0.5,0.02))
mesh_house4 = Mesh("../../data/mesh_house.xml.gz")
mesh_house4.translate(Point(0.19,0.5,0.012))
mesh_house5 = Mesh("../../data/mesh_house.xml.gz")
mesh_house5.translate(Point(0.241,0.345,0.026))
mesh_house6 = Mesh("../../data/mesh_house.xml.gz")
mesh_house6.translate(Point(0.28,0.28,0.008))
mesh_house7 = Mesh("../../data/mesh_house.xml.gz")
mesh_house7.translate(Point(0.5,0.15,0.015))
mesh_house8 = Mesh("../../data/mesh_house.xml.gz")
mesh_house8.translate(Point(0.4,0.25,0.02))
mesh_house9 = Mesh("../../data/mesh_house.xml.gz")
mesh_house9.translate(Point(0.72,0.15,0.015))
meshes = (mesh_air, mesh_house, mesh_house1, mesh_house2,  mesh_house3, mesh_house4, mesh_house5, mesh_house6, mesh_house7, mesh_house8, mesh_house9)


image_dim = (500, 500)
camera = (0.48,0.28,0.051)
south_normal = (-1.0, 0.0, 0.0)
screen_distance = 0.012
num_of_images = 32
screen_height = 0.01
views = []

V = get_360_view(meshes, image_dim, camera, south_normal,
                    screen_distance, num_of_images, screen_height, save_image = False)

print "View rating =", V
