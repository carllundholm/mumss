# Gives a rating between 0 and 1, based on how good the view is from the house

from mumss import *

# Simple test
mesh_air = Mesh("../data/mesh_air.xml.gz")
mesh_house = Mesh("../data/mesh_house.xml.gz")
mesh_house.translate(Point(0.6,0.3,0.03))
meshes = (mesh_air, mesh_house)

image_dim = (800, 500)
camera = (0.5,0.3,0.055)
south_normal = (1.0, 0.0, 0.0)
screen_distance = 0.012
num_of_images = 8
screen_height = 0.01
views = []

V = get_360_view(meshes, image_dim, camera, south_normal,
                    screen_distance, num_of_images, screen_height, save_image = False)

print "View rating =", V
