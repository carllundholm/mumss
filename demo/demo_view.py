# Gives a sequence of images

from mumss import *
from numpy import zeros, rot90
from PIL import Image

# Simple test
mesh_air = Mesh("../data/mesh_air.xml.gz")
mesh_house = Mesh("../data/mesh_house.xml.gz")
mesh_house.translate(Point(0.6,0.3,0.03))
meshes = (mesh_air, mesh_house)

image_dim = (800, 500)
camera = (0.5,0.3,0.055)
normal = (1.0, 0.0, 0.0)
screen_distance = 0.012
screen_width = 0.012
screen_height = 0.01
number_of_images = 16

for i in range(number_of_images):

    angle = 2*DOLFIN_PI*i/number_of_images
    normal = (cos(angle), sin(angle), 0.0)

    render_view(meshes, image_dim, camera, normal,
                screen_distance, screen_width, screen_height,
                '%03d.png' % i, 'output')
