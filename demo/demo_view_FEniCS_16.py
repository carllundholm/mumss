# Produces the movie, which is used at FEniCS'16 meeting. 

from mumss import *
from numpy import zeros, rot90
from PIL import Image
import os

# Simple test
mesh_air = Mesh("../data/mesh_air.xml.gz")
mesh_house = Mesh("../data/mesh_house.xml.gz")
mesh_house.translate(Point(0.5,0.3,0.047))
mesh_house1 = Mesh("../data/mesh_house.xml.gz")
mesh_house1.translate(Point(0.65,0.45,0.01))
mesh_house2 = Mesh("../data/mesh_house.xml.gz")
mesh_house2.translate(Point(0.55,0.50,0.01))
mesh_house3 = Mesh("../data/mesh_house.xml.gz")
mesh_house3.translate(Point(0.4,0.5,0.02))
mesh_house4 = Mesh("../data/mesh_house.xml.gz")
mesh_house4.translate(Point(0.2,0.5,0.005))
mesh_house5 = Mesh("../data/mesh_house.xml.gz")
mesh_house5.translate(Point(0.25,0.35,0.015))
mesh_house6 = Mesh("../data/mesh_house.xml.gz")
mesh_house6.translate(Point(0.28,0.28,0.005))
mesh_house7 = Mesh("../data/mesh_house.xml.gz")
mesh_house7.translate(Point(0.5,0.15,0.015))
mesh_house8 = Mesh("../data/mesh_house.xml.gz")
mesh_house8.translate(Point(0.4,0.25,0.02))
mesh_house9 = Mesh("../data/mesh_house.xml.gz")
mesh_house9.translate(Point(0.72,0.15,0.015))
meshes = (mesh_air, mesh_house, mesh_house1, mesh_house2,  mesh_house3, mesh_house4, mesh_house5, mesh_house6, mesh_house7, mesh_house8, mesh_house9)

image_dim = (1024, 640)
camera = (0.486,0.281,0.0505) #(0.48,0.28,0.051)
screen_distance = 0.012
screen_width = 0.012
screen_height = 0.01
number_of_images = 1000
start_angle = DOLFIN_PI/2

for i in range(number_of_images):

    angle = 2*DOLFIN_PI*i/number_of_images+start_angle
    normal = (cos(angle), sin(angle), 0.0)

    render_view(meshes, image_dim, camera, normal,
                screen_distance, screen_width, screen_height,
                '%03d.png' % i, 'output')

# YOU NEED FFMPEG:
# $ brew install ffmpeg

os.system("ffmpeg -r 25 -i output/%03d.png -vcodec libx264 -pix_fmt yuv420p movie.mp4")  
