#include <iostream>
#include <stdexcept>
#include <cmath>
#include <vector>
#include <limits>

template<typename T>
T round(T v) {
  return int(v + 0.5);
}

// Returns positive values if point is on right side of the line
// between v and v and negative values if on left side
double _edge_function(double px, double py,
                      double vx, double vy, double vz,
                      double wx, double wy, double wz)
{
  return (px - vx)*(wy - vy) - (py - vy)*(wx - vx);
}

// Returns depth for point p where px is x coordinate in rasterspace
double _find_depth(double px,
                   double vz, double vdepth,
                   double wz, double wdepth)
{
  const double q = (px - vz)/(wz - vz);
	return 1. / (1. / vdepth*(1. - q) + 1 / wdepth*q);
}

// Raster/image coord
std::pair<double, double> _toRaster(double x, double y, double z,
                                    double screen_distance, int image_width, int image_height,
                                    double screen_width, double screen_height)
{
  const double screen_x = screen_distance * x / y;    // x coor. in screen
  const double screen_z = screen_distance * z / y;    // z coor. in screen
  const double NDC_x = 2. * screen_x / screen_width;  // x coor. in NDC (range(-1,1))
  const double NDC_z = 2. * screen_z / screen_height; // z coor. in NDC
  return std::make_pair((NDC_x + 1.) / 2. * image_width, (1. - NDC_z) / 2. * image_height);
};

// Return index of maximum
int _maxi(double a, double b, double c)
{
  if (a > b && a > c)
    return 0;
  if (b > c)
    return 1;
  return 2;
}

// Return index of mainimum
int _mini(double a, double b, double c)
{
  if (a < b && a < c)
    return 0;
  if (b < c)
    return 1;
  return 2;
}

double _rotate_x(double ux, double uy, double uz, double px, double py, double pz, double theta)
{
  const double a = std::cos(theta);
  const double b = std::sin(theta);
  return (a + ux*ux*(1. - a))*px + (ux*uy*(1. - a) - uz*b)*py + (ux*uz*(1. - a) + uy*b)*pz;
};

double _rotate_y(double ux, double uy, double uz, double px, double py, double pz, double theta)
{
  const double a = std::cos(theta);
  const double b = std::sin(theta);
  return (uy*ux*(1. - a) + uz*b)*px + (a + uy*uy*(1. - a))*py + (uy*uz*(1. - a) - ux*b)*pz;
};

double _rotate_z(double ux, double uy, double uz, double px, double py, double pz, double theta)
{
  const double a = std::cos(theta);
  const double b = std::sin(theta);
  return (uz*ux*(1. - a) - uy*b)*px + (uz*uy*(1. - a)+ux*b)*py + (a+uz*uz*(1. - a))*pz;
};

double render_view(int*    image,       int image_size,
                   double* coordinates, int coordinates_size,
                   int*    triangles,   int triangles_size,
                   double* types,     int types_size,
                   int image_width, int image_height,
                   double camera_x, double camera_y, double camera_z,
                   double normal_x, double normal_y, double normal_z,
                   double screen_distance,
                   double screen_width, double screen_height, double max_dist)
{

  // Check arguments
  if (coordinates_size % 3 != 0)
    throw std::invalid_argument("Length of coordinate array must be a multiple of 3.");
  if (triangles_size % 3 != 0)
    throw std::invalid_argument("Length of triangles array must be a multiple of 3.");
  if (3*types_size != triangles_size)
    throw std::invalid_argument("Length of types array does not match number of triangles.");
  if (image_width*image_height != image_size/3)
    throw std::invalid_argument("Image width and height does not match size of RGB image array.");

	// Scale normal to unit normal
	const double len_normal = std::sqrt(normal_x*normal_x + normal_y*normal_y + normal_z*normal_z);
	if (len_normal < 0.99999 || len_normal > 1.00001)
	{
		normal_x = normal_x/len_normal;
		normal_y = normal_y/len_normal;
		normal_z = normal_z/len_normal;
	}

  // Compute number of triangles
  const int num_triangles = triangles_size / 3;

  // Standard depth direction (Here it is in the y-direction)
  const double dx = 0.;
  const double dy = 1.;
  const double dz = 0.;


	//  Normal for the temperary y-axis
	const double len_temp_normal = std::sqrt(normal_x*normal_x + normal_y*normal_y);
	const double temp_norm_x = normal_x/len_temp_normal;
	const double temp_norm_y = normal_y/len_temp_normal;
	const double temp_norm_z = 0.;

	// Rotation axis (z-axis) (Cross product)
  double ux = temp_norm_y*dz - temp_norm_z*dy;
  double uy = temp_norm_z*dx - temp_norm_x*dz;
  double uz = temp_norm_x*dy - temp_norm_y*dx;

  if (dx == temp_norm_x && dy == temp_norm_y && dz == temp_norm_z)
  {
    ux = 0.;
    uy = 0.;
    uz = 1.;
  }
	else if (dx == -temp_norm_x && dy == -temp_norm_y && dz == -temp_norm_z)
	{
    ux = 0.;
    uy = 0.;
    uz = -1.;
	}

	// Make rotation axis a unit vector
	const double len_rot_normal = std::sqrt(ux*ux + uy*uy + uz*uz);
	if (len_rot_normal < 0.99999 || len_rot_normal > 1.00001)
	{
		ux = ux/len_rot_normal;
		uy = uy/len_rot_normal;
		uz = uz/len_rot_normal;
	}

  // Rotation angle around the z-axis
  const double theta_horizontal = std::acos(dx*temp_norm_x + dy*temp_norm_y + dz*temp_norm_z);


	// Rotation axis (the x-axis) for the vertical rotation
	double u1x = -1;
	double u1y = 0;
	double u1z = 0;
	if (normal_z < 0.)
	{
		u1x = 1;
	}


	// Rotation angle around the new x-axis
	double theta_vertical = std::acos(normal_x*temp_norm_x + normal_y*temp_norm_y + normal_z*temp_norm_z);

	// Check if theta_vertical = Nan
	if (theta_vertical != theta_vertical)
	{
		theta_vertical = 0.;
	}

  // Initialize depth buffer
  std::vector<double> depth_buffer(image_size/3);
  std::fill(depth_buffer.begin(), depth_buffer.end(), std::numeric_limits<double>::max());


  // Iterate over triangles
  for (int t = 0; t < num_triangles; t++)
  {

    // Get coordinates for vertecies and move them with respect to camera position
    const double v0x_org = coordinates[triangles[t*3]*3]-camera_x;
    const double v0y_org = coordinates[triangles[t*3]*3+1]-camera_y;
    const double v0z_org = coordinates[triangles[t*3]*3+2]-camera_z;
    const double v1x_org = coordinates[triangles[t*3+1]*3]-camera_x;
    const double v1y_org = coordinates[triangles[t*3+1]*3+1]-camera_y;
    const double v1z_org = coordinates[triangles[t*3+1]*3+2]-camera_z;
    const double v2x_org = coordinates[triangles[t*3+2]*3]-camera_x;
    const double v2y_org = coordinates[triangles[t*3+2]*3+1]-camera_y;
    const double v2z_org = coordinates[triangles[t*3+2]*3+2]-camera_z;


    // Rotate coordinates in the horizontal plan (around orginal z-axis)
    double v0x_ = _rotate_x(ux, uy, uz, v0x_org, v0y_org, v0z_org, theta_horizontal);
    double v0y_ = _rotate_y(ux, uy, uz, v0x_org, v0y_org, v0z_org, theta_horizontal);
		double v0z_ = _rotate_z(ux, uy, uz, v0x_org, v0y_org, v0z_org, theta_horizontal);
    double v1x_ = _rotate_x(ux, uy, uz, v1x_org, v1y_org, v1z_org, theta_horizontal);
    double v1y_ = _rotate_y(ux, uy, uz, v1x_org, v1y_org, v1z_org, theta_horizontal);
		double v1z_ = _rotate_z(ux, uy, uz, v1x_org, v1y_org, v1z_org, theta_horizontal);
    double v2x_ = _rotate_x(ux, uy, uz, v2x_org, v2y_org, v2z_org, theta_horizontal);
    double v2y_ = _rotate_y(ux, uy, uz, v2x_org, v2y_org, v2z_org, theta_horizontal);
		double v2z_ = _rotate_z(ux, uy, uz, v2x_org, v2y_org, v2z_org, theta_horizontal);


		// Rotate coordinates in the vertial plan (around new x-axis)
		double v0x = _rotate_x(u1x, u1y, u1z, v0x_, v0y_, v0z_, theta_vertical);
		double v0y = _rotate_y(u1x, u1y, u1z, v0x_, v0y_, v0z_, theta_vertical);
		double v0z = _rotate_z(u1x, u1y, u1z, v0x_, v0y_, v0z_, theta_vertical);
		double v1x = _rotate_x(u1x, u1y, u1z, v1x_, v1y_, v1z_, theta_vertical);
		double v1y = _rotate_y(u1x, u1y, u1z, v1x_, v1y_, v1z_, theta_vertical);
		double v1z = _rotate_z(u1x, u1y, u1z, v1x_, v1y_, v1z_, theta_vertical);
		double v2x = _rotate_x(u1x, u1y, u1z, v2x_, v2y_, v2z_, theta_vertical);
		double v2y = _rotate_y(u1x, u1y, u1z, v2x_, v2y_, v2z_, theta_vertical);
		double v2z = _rotate_z(u1x, u1y, u1z, v2x_, v2y_, v2z_, theta_vertical);


    // Check if the triangle is in front of the screen
    if (v0y <= 0 || v1y <= 0 || v2y <= 0)
      continue;

    //Translate points into Rasterspace, loop over all vertices in triangle
    std::pair<double, double> v0_raster = _toRaster(v0x, v0y, v0z,
                                                    screen_distance, image_width, image_height,
                                                    screen_width, screen_height);
    std::pair<double, double> v1_raster = _toRaster(v1x, v1y, v1z,
                                                    screen_distance, image_width, image_height,
                                                    screen_width, screen_height);
    std::pair<double, double> v2_raster = _toRaster(v2x, v2y, v2z,
                                                    screen_distance, image_width, image_height,
                                                    screen_width, screen_height);

    // Find mapped triangle coordinates
    const double xs[3] = {v0_raster.first, v1_raster.first, v2_raster.first};
    const double ys[3] = {v0y, v1y, v2y};
    const double zs[3] = {v0_raster.second, v1_raster.second, v2_raster.second};


    // Find bounding box
    int xmaxi = _maxi(xs[0], xs[1], xs[2]);
    int xmini = _mini(xs[0], xs[1], xs[2]);
    int zmaxi = _maxi(zs[0], zs[1], zs[2]);
    int zmini = _mini(zs[0], zs[1], zs[2]);
    double xmax = xs[xmaxi];
    double xmin = xs[xmini];
    double zmax = zs[zmaxi];
    double zmin = zs[zmini];

    // Check if triangle is outside screen
    if (xmin > image_width - 1 || xmax < 0 || zmin > image_height - 1 || zmax < 0)
      continue;

    xmin = std::max(0.0, xmin);
    xmax = std::min(double (image_width-1), xmax);
    zmin = std::max(0.0, zmin);
    zmax = std::min(double (image_height-1), zmax);

    // Loop over pixels
    for (int z = round(zmin); z <= round(zmax); z++)
    {
      bool first_pixel_found = false;
      const int xstart = round(std::min(std::max(1.,xs[zmaxi]),double (image_width-1)));

      // Check pixels above the middle
      for (int x = xstart; x <= round(xmax); x++)
      {
        double zpix = z + 0.5;
        double xpix = x + 0.5;
        double w0 = _edge_function(xpix, zpix, xs[0], zs[0], v0y, xs[1], zs[1], v1y);
        double w1 = _edge_function(xpix, zpix, xs[1], zs[1], v1y, xs[2], zs[2], v2y);
        double w2 = _edge_function(xpix, zpix, xs[2], zs[2], v2y, xs[0], zs[0], v0y);


        // Mark pixels inside triangle
        if ((w0<=0 && w1<=0 && w2<=0) || (w0>=0 && w1>=0 && w2>=0))
        {
          first_pixel_found = true;
          double p_ydist = _find_depth(zpix, zs[zmini], ys[zmini], zs[zmaxi], ys[zmaxi]);
          const int k = z*image_width + x;

          if (p_ydist < depth_buffer[k])
          {
            image[k] = t;
            depth_buffer[k] = p_ydist;
          }
        }
        else if (first_pixel_found)
          break;
      }

      // Check pixels below the middle
      for (int x = xstart - 1; x >= round(xmin); x--)
      {
        double zpix = z + 0.5;
        double xpix = x + 0.5;
        double w0 = _edge_function(xpix, zpix, xs[0], zs[0], v0y, xs[1], zs[1], v1y);
        double w1 = _edge_function(xpix, zpix, xs[1], zs[1], v1y, xs[2], zs[2], v2y);
        double w2 = _edge_function(xpix, zpix, xs[2], zs[2], v2y, xs[0], zs[0], v0y);

        // Mark pixels inside triangle
        if ((w0<=0 && w1<=0 && w2<=0) || (w0>=0 && w1>=0 && w2>=0))
        {

          first_pixel_found = true;
          double p_ydist = _find_depth(zpix, zs[zmini], ys[zmini], zs[zmaxi], ys[zmaxi]);
          const int k = z*image_width + x;
					if (p_ydist < depth_buffer[k])
          {
            image[k] = t;
            depth_buffer[k] = p_ydist;
          }
        }
        else if (first_pixel_found)
          break;
      }
    }
  }


  // Calculate the view integral

	// Weights for location rating
	double w_island = 0.7;
	double w_air = 1.0;
	double w_sea = 1.0;
	double w_house = 0.1;
	// Types: 1 Island/ground, 2 sea, 3 air, 4 roof, 5 house walls

  double view = 0.0;
  double dist = 0.518; // To scale to kilometers. As the mesh have been scaled to unit size
  double L = 0.217; // If a item has weight 1, and 5 km is the max length you can see, then L=0.17 for the view of a house to be 0.9 or 90% okay.
  int undefined_pixels = 0;

  for (int i = 0; i < image_size/3; i++)
  {
    if(depth_buffer[i] > (std::numeric_limits<double>::max()/100.))
    {
      // If pixel not set
      undefined_pixels++;
      image[i] = 0;
    }
    else if (types[image[i]] == 1) 		//Island
    {
      view += 1. - std::exp(-w_island*depth_buffer[i]*dist/L);
			image[i] = 0;																											//Red
			image[i+image_size/3] = 240*(1.-depth_buffer[i]/max_dist);		//Green
			image[i+2*image_size/3] = 70*(1.-depth_buffer[i]/max_dist);		//Blue
    }
    else if (types[image[i]] == 2) 		//Sea
    {
      view += w_sea;
			image[i] = 28;									//Red
			image[i+image_size/3] = 107;		//Green
			image[i+2*image_size/3] = 160;	//Blue
    }
    else if (types[image[i]] == 3) 		//Air
    {
      view += w_air;
			image[i] = 0;										//Red
			image[i+image_size/3] = 191;		//Green
			image[i+2*image_size/3] = 255;	//Blue
    }
    else if (types[image[i]] == 4) 		//Roof
    {
      view += 1. - std::exp(-w_house*depth_buffer[i]*dist/L);
			image[i] = 80*(1.-depth_buffer[i]/max_dist);	//Red
			image[i+image_size/3] = 0;										//Green
			image[i+2*image_size/3] = 0;									//Blue
    }
    else 															//House walls
    {
      view += 1. - std::exp(-w_house*depth_buffer[i]*dist/L);
			image[i] = 200*(1.-depth_buffer[i]/max_dist);								//Red
			image[i+image_size/3] = 10*(1.-depth_buffer[i]/max_dist);		//Green
			image[i+2*image_size/3] = 10*(1.-depth_buffer[i]/max_dist);	//Blue
    }
  }

	// Count undefined pixels (Could appear if a part of the screen is outside the domain)
  if (undefined_pixels > 0)
    std::cout << undefined_pixels << " undefined pixels" << std::endl;

  view = view/(double)(image_size/3-undefined_pixels);

  return view;
}
