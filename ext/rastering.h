double render_view(int*    image,       int image_size,
                   double* coordinates, int coordinates_size,
                   int*    triangles,   int triangles_size,
                   double* weights,     int weights_size,
                   int image_width, int image_height,
                   double camera_x, double camera_y, double camera_z,
                   double normal_x, double normal_y, double normal_z,
                   double screen_distance,
                   double screen_width, double screen_height, double max_dist);
