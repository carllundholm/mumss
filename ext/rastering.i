%module rastering
%{
    #define SWIG_FILE_WITH_INIT
    #include "rastering.h"
%}

// Use numpy typemaps
%include "numpy.i"
%init %{
  import_array();
%}

// Input typemaps
%apply (double* IN_ARRAY1, int DIM1) {(double* coordinates, int coordinates_size)}
%apply (int*    IN_ARRAY1, int DIM1) {(int* triangles,      int triangles_size)}
%apply (double* IN_ARRAY1, int DIM1) {(double* weights,     int weights_size)}

// Output typemaps
%apply (int* INPLACE_ARRAY1, int DIM1) {(int* image, int image_size)}

// Wrap code
%include "rastering.h"
