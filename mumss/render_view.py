from fenics import *

from numpy import zeros, ascontiguousarray
from _rastering import render_view as _render_view
from PIL import Image, ImageDraw, ImageFont
from os import path, makedirs
from stokessolver import _compute_bounding_box

class WallDomain(SubDomain):
    # def __init__(self, xmin, xmax, ymin, ymax, zmin, zmax):
    #     self.xmin = xmin
    #     self.xmax = xmax
    #     self.ymin = ymin
    #     self.ymax = ymax
    #     self.zmin = zmin
    #     self.zmax = zmax
    def inside(self, x, on_boundary):
        # FIXME: take xmin, xmax,... as input
        return near(x[0],xmin,DOLFIN_EPS*10) or near(x[0],xmax,DOLFIN_EPS*10) or near(x[1],ymin,DOLFIN_EPS*10) or near(x[1],ymax,DOLFIN_EPS*10) or near(x[2],zmax,DOLFIN_EPS*10)

class SeaDomain(SubDomain):
    # def __init__(self, zmin):
    #     self.zmin = zmin
    def inside(self, x, on_boundary):
        # FIXME: take xmin, xmax,... as input
        return near(x[2],zmin,DOLFIN_EPS*10)

class HouseWallDomain(SubDomain):
    # def __init__(self, xmin, xmax, ymin, ymax, zmin):
    #     self.xmin = xmin
    #     self.xmax = xmax
    #     self.ymin = ymin
    #     self.ymax = ymax
    #     self.zmin = zmin
    def inside(self, x, on_boundary):
        # FIXME: take xmin, xmax,... as input
        return near(x[0],xmin,DOLFIN_EPS*10) or near(x[0],xmax,DOLFIN_EPS*10) or near(x[1],ymin,DOLFIN_EPS*10) or near(x[1],ymax,DOLFIN_EPS*10) or near(x[2],zmin,DOLFIN_EPS*10)



def get_360_view(meshes, image_dim, camera, south_direction, screen_distance, num_of_images, screen_height, save_image=False):
    try:
       val = int(num_of_images)
       if num_of_images < 4:  
           raise ValueError("Sorry, number of images must be higher than 4")
    except ValueError:
       print("Number of images is not an int")
       
    screen_width = 2*tan(DOLFIN_PI/num_of_images)*screen_distance
    total_view = 0
    sumw = 0
    for i in range(num_of_images):
        angle = 2*DOLFIN_PI*i/num_of_images
        normal = (south_direction[0]*cos(angle)-south_direction[1]*sin(angle), south_direction[0]*sin(angle)+south_direction[1]*cos(angle), 0.0)
        view = render_view(meshes, image_dim, camera, normal, screen_distance, screen_width, screen_height, '%03d.png' % i, 'output', save_image)
        weight = 2.*DOLFIN_PI/num_of_images*(1.+(1./2.)*sin(angle-3.*DOLFIN_PI*(1./2.)))/(2.*DOLFIN_PI) #Weights south 3 times as high as north. All weigths sums to one.
        total_view += view*weight
    return total_view
    

def render_view(meshes, image_dim, camera, normal, screen_distance, screen_width, screen_height, filename=None, directory=None, save_image=True):
    # OBS! First mesh needs to be a island_box mesh.
    
    # Create boundary meshes
    boundary_meshes = [BoundaryMesh(mesh, "exterior") for mesh in meshes]
    # FIXME: take xmin, xmax,... as input to wall domain, so that xmin, xmax,.. do not need to be global.
    global xmin
    global xmax
    global ymin
    global ymax 
    global zmin 
    global zmax
    (xmin, xmax, ymin, ymax, zmin, zmax, xd, yd, zd) = _compute_bounding_box(meshes[0])

    # maximal distance from camera to boundary of the bounding box
    xcam_dist = max(xmax-camera[0], camera[0]-xmin)
    ycam_dist = max(ymax-camera[1], camera[1]-ymin)
    zcam_dist = max(zmax-camera[2], camera[2]-zmin)
    max_dist = sqrt(xcam_dist*xcam_dist+ycam_dist*ycam_dist+zcam_dist*zcam_dist)
    
    domain_types = CellFunction("double", boundary_meshes[0])
    domain_types.set_all(1)            # Island/ground 
    sea_domain = SeaDomain()
    sea_domain.mark(domain_types, 2)    # sea 
    wall_domain = WallDomain()
    wall_domain.mark(domain_types, 3)    # Air 

    # Types: 1 Island/ground, 2 sea, 3 air, 4 roof, 5 house walls
    
    
    # Compute sizes
    num_coordinates = sum(b.num_vertices() for b in boundary_meshes)
    num_triangles   = sum(b.num_cells() for b in boundary_meshes)

    # Prepare data
    image = zeros(image_dim[0]*image_dim[1]*3, dtype='int32')
    coordinates = zeros(3*num_coordinates, dtype='double')
    triangles = zeros(3*num_triangles, dtype='int32')
    types = zeros(num_triangles, dtype='int32')

    # Extract data
    offset_t = 0
    offset_v = 0
    offset_w = 0
    for i, b in enumerate(boundary_meshes):
        if i > 0:
            domain_types = CellFunction("double", boundary_meshes[i])
            domain_types.set_all(4) # Roof
            (xmin, xmax, ymin, ymax, zmin, zmax, xd, yd, zd) = _compute_bounding_box(meshes[i]) # FIXME only works for rectangle houses
            wall_domain = HouseWallDomain()
            wall_domain.mark(domain_types, 5)    # House walls

        # Add triangles
        nt = b.num_cells()
        triangles[offset_t:offset_t + 3*nt] = b.cells().flatten() + offset_v/3
        offset_t += 3*nt

        # Add coordinates
        nv = b.num_vertices()
        coordinates[offset_v:offset_v + 3*nv] = b.coordinates().flatten()
        offset_v += 3*nv
        

        # Add weights
        nw = b.num_cells()
        if i == 0:
            # weights[offset_w:offset_w + nw] = domain_weights.array()   # Air, sea and island weights
            types[offset_w:offset_w + nw] = domain_types.array()
        else:
            # weights[offset_w:offset_w + nw] = w_house               # House weight
            types[offset_w:offset_w + nw] = domain_types.array()
        offset_w += nw

    
    
    
    # Call rendering function
    t = time()
    view = _render_view(image, coordinates, triangles, types,
                        image_dim[0], image_dim[1],
                        camera[0], camera[1], camera[2],
                        normal[0], normal[1], normal[2],
                        screen_distance, screen_width, screen_height, max_dist)

    # Save image to file
    if save_image:
        if  directory != None and not path.exists(directory):
            makedirs(directory)
        
        image_out = image.reshape((3,image_dim[1],image_dim[0]))
        image_out = ascontiguousarray(image_out.transpose(1,2,0))
        img = Image.fromarray((image_out).astype('uint8'),'RGB')
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype("Arial.ttf", 40)
        draw.text((0, 0),"View: %g" %view,(100,100,100),font=font) # Write the view valuation on the image
        if directory == None:
            img.save((filename))
        else:
            img.save(path.join(directory, filename))

    t = time() - t
    info("View rendered in %s seconds." % t)

    return view
