# Simulate stokes on mesh_air.xml and mesh_housebox.xml
# Here mesh_housebox.xml has mesh in the whole box inclusive the house
# The boundaries of the house are marked as 1 in mesh_housebox_facet_region.xml
# Further work: The house boundaries should be noslip boundaries.

from dolfin import *
from mshr import *
from Rasterization import RasterView
import Image
import time

class InflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.0)

class OutflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.0)

class NoslipBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not (near(x[0], 0.0) or near(x[0], 1.0))

class Ground(SubDomain):
    def inside(self, x, on_boundary):
        return not air_tree.collides(Point(x[0],x[1],x[2]))


if MPI.size(mpi_comm_world()) > 1:
    info("Sorry, this demo does not (yet) run in parallel.")
    exit(0)

# Create meshes
print 'Reading and building meshes...'
mesh_air = Mesh("meshes/mesh_air.xml")
mesh_housebox = Mesh("meshes/mesh_housebox.xml")

xs = zip(*mesh_air.coordinates())
xmin = min(xs[0])
xmax = max(xs[0])
ymin = min(xs[1])
ymax = max(xs[1])
zmin = min(xs[2])
zmax = max(xs[2])
xd = xmax - xmin; yd = ymax - ymin; zd = zmax - zmin 

mesh_housebox.translate(Point(xd/2+0.1,yd/2,zd/5))

# Save the mesh
File("meshes/mesh_housebox.pvd") << mesh_housebox
File("meshes/mesh_air.pvd") << mesh_air

# Build multimesh
multimesh = MultiMesh()
multimesh.add(mesh_air)
multimesh.add(mesh_housebox)
multimesh.build()

print 'Setting the scene..'
# Create function space
P2 = VectorElement("Lagrange", tetrahedron, 2)
P1 = FiniteElement("Lagrange", tetrahedron, 1)
TH = P2 * P1
W  = MultiMeshFunctionSpace(multimesh, TH)

# Define trial and test functions and right-hand side
f = Constant((0, 0, 0))

# Define trial and test functions and right-hand side
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)

# Define facet normal and mesh size
n = FacetNormal(multimesh)
h = 2.0*Circumradius(multimesh)

# Parameters
alpha = 400.0

def tensor_jump(v, n):
    return outer(v('+'), n('+')) + outer(v('-'), n('-'))

def a_h(v, w):
    return inner(grad(v), grad(w))*dX \
         - inner(avg(grad(v)), tensor_jump(w, n))*dI \
         - inner(avg(grad(w)), tensor_jump(v, n))*dI \
         + alpha/avg(h) * inner(jump(v), jump(w))*dI

def b_h(v, q):
    return -div(v)*q*dX + jump(v, n)*avg(q)*dI

def l_h(v, q, f):
    return inner(f, v)*dX

def s_O(v, w):
    return inner(jump(grad(v)), jump(grad(w)))*dO

def s_C(v, q, w, r):
    return h*h*inner(-div(grad(v)) + grad(q), -div(grad(w)) - grad(r))*dC

def l_C(v, q, f):
    return h*h*inner(f, -div(grad(v)) - grad(q))*dC

# Define bilinear form
a = a_h(u, v) + b_h(v, p) + b_h(u, q) + s_O(u, v) + s_C(u, p, v, q)

# Define linear form
L  = l_h(v, q, f) + l_C(v, q, f)

# Assemble linear system
A = assemble_multimesh(a)
b = assemble_multimesh(L)

# Create boundary values
in_exp = "sin((x[1] - ymin)*DOLFIN_PI/yd)*sin((x[2] - zmin)*DOLFIN_PI/zd)"
inflow_value = Expression((in_exp, "0.0", "0.0"), \
             ymin = ymin, zmin = zmin, yd = yd, zd = zd)
outflow_value = Constant(0)
noslip_value = Constant((0, 0, 0))

# Create subdomains for boundary conditions
inflow_boundary = InflowBoundary()
outflow_boundary = OutflowBoundary()
noslip_boundary = NoslipBoundary()

# Create subspaces for boundary conditions
V = MultiMeshSubSpace(W, 0)
Q = MultiMeshSubSpace(W, 1)

# Create boundary conditions
bc0 = MultiMeshDirichletBC(V, noslip_value,  noslip_boundary)
bc1 = MultiMeshDirichletBC(V, inflow_value,  inflow_boundary)
bc2 = MultiMeshDirichletBC(Q, outflow_value, outflow_boundary)

# Apply boundary conditions
bc0.apply(A, b)
bc1.apply(A, b)
bc2.apply(A, b)

# Create subdomains for sink domains
air_tree = BoundingBoxTree()
air_tree.build(mesh_air)
ground = Ground()

# Create sink domains
sd_ground = MultiMeshDirichletBC(V, noslip_value, ground)

# Apply sink domains
print 'Solving..'
sd_ground.apply(A, b)

# Add noslip to house boundary
ff_house = MeshFunction('size_t', mesh_housebox, 'meshes/mesh_housebox_facet_region.xml')
sd_house_boundary = MultiMeshDirichletBC(V, noslip_value, ff_house, 1, 1)
sd_house_boundary.apply(A, b)

# Compute solution
w = MultiMeshFunction(W)
solve(A, w.vector(), b)

# Extract solution components
u_air = w.part(0).sub(0)
u_hb = w.part(1).sub(0)
p_air = w.part(0).sub(1)
p_hb = w.part(1).sub(1)

# Save to file
File("solutions_sinkmesh/u_air.pvd") << u_air
File("solutions_sinkmesh/u_hb.pvd") << u_hb
File("solutions_sinkmesh/p_air.pvd") << p_air
File("solutions_sinkmesh/p_hb.pvd") << p_hb

# Plot solution
# plot(W.multimesh())
# plot(mesh_0)
# plot(u_air, title="u_air", interactive=True)
# plot(u_hb, title="u_hb", interactive=True)
# plot(p0, title="p_0")
# plot(p1, title="p_1")
# interactive()



# Show island
print 'Getting ready for rendering..'
# make wall and island domains
class WallDomain(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (near(x[0],xmin) or near(x[0],xmax) or near(x[1],ymin,DOLFIN_EPS*10) or near(x[1],ymax) or near(x[2],zmax))

class BoundaryDomain(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

class WaterDomain(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[2],zmin)
        
boundary = BoundaryDomain()
wall = WallDomain()
water = WaterDomain()
ff = FacetFunction('size_t', mesh_air)
boundary.mark(ff, 2)
wall.mark(ff, 1)
water.mark(ff, 3)
# plot(ff)
# interactive()


# Redering
meshes = [mesh_air,mesh_housebox]
facetFunctions = [ff,ff_house]
markers = [[1,2,3],[1]]
colors = [[190,125,250],[50]]       # Color between 0 and 255
image_width = 0.2
image_height = 0.1
pix_width = 500
pix_height = 500
dist_to_screen = 0.1 
rotation_angle = DOLFIN_PI/2.       # Rotate camera
cam_point = [xd*0.4,yd/2.,zd*0.6]   # Camera position
max_dist = max([xd,yd,zd])

print 'Making image...'
t0 = time.clock()
image = RasterView(meshes, facetFunctions, markers, colors, image_width, image_height, pix_width, pix_height, dist_to_screen, rotation_angle, cam_point, max_dist)
print time.clock() - t0, "seconds process time"
img = Image.fromarray((image).astype('uint8'))
img.save('out.png')
