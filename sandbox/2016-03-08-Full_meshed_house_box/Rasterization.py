from dolfin import *
import numpy as np

def RasterView(meshes, facetFunctions, markers, colors, image_width, image_height, pix_width, pix_height, dist_to_screen, rotation_angle, cam_point, max_dist):

    # Set screen
    image = np.zeros((pix_height,pix_width))
    depthBuffer = np.empty((pix_height,pix_width))
    depthBuffer[:] = np.inf
    l_ = -image_width/2.
    r_ = image_width/2.
    b_ = -image_height/2.
    t_ = image_height/2.
    w = np.zeros(3)
    vs = np.zeros((3,3))

    # Returns positive values if point is on right side of the line
    # between v1 and v2 and negative values if on left side.
    def edgeFunction(v1, v2, p):
        return (p[0] - v1[0]) * (v2[1] - v1[1]) - (p[1] - v1[1]) * (v2[0] - v1[0])

    # Returns depth for point p. Where px is x coordinate in rasterspace
    def findDepth(px, V0, V1):
        q = (px - V0[1])/(V1[1] - V0[1])
        return 1./(1./V0[2]*(1.-q) + 1/V1[2]*q)

    # Converts coordinates to raster coordinates
    def toRaster(c):
        [x,y,z] = c
        screen_x = dist_to_screen * x / y    # x coor. in screen
        screen_z = dist_to_screen * z / y    # z coor. in screen
        NDC_x = 2. * screen_x / (r_ - l_) - (r_ + l_) / (r_ - l_) # x coor. in NDC (range(-1,1))
        NDC_z = 2. * screen_z / (t_ - b_) - (t_ + b_) / (t_ - b_) # z coor. in NDC
        return [(NDC_x + 1.) / 2. * pix_width, (1. - NDC_z) / 2. * pix_height, y]   # Raster/image coord

    for imesh in range(len(meshes)):
        mesh = meshes[imesh]
        facetFunction = facetFunctions[imesh]
    
        # Translate all points
        coor = mesh.coordinates()
        coor = np.array([i-cam_point for i in coor])

        # Rotate coordinate system
        s = sin(rotation_angle)
        c = cos(rotation_angle)
        coor = np.array([[x*c-y*s,x*s+y*c,z] for [x,y,z] in coor])
    
        for imarker in range(len(markers[imesh])):  
            marker = markers[imesh][imarker]
            color = colors[imesh][imarker]

            facets1 = np.where(facetFunction.array()==marker)
            # Loop over all marked triangles
            for f in facets1[0]:
                vf = Facet(mesh,f).entities(0)
            
                if all(c <= dist_to_screen for c in coor[vf,1]):
                    continue
            
                # Translate points into Rasterspace, loop over all vertices in triangle
                vs[0] = toRaster(coor[vf[0]])
                vs[1] = toRaster(coor[vf[1]])
                vs[2] = toRaster(coor[vf[2]])
            
                # Mark all pixels inside the triangle and find distance
                # Find surroundbox for triangle
                vsT = zip(*vs)
                xraster = vsT[0]
                zraster = vsT[1]
                xminras = min(xraster)
                xmaxras = max(xraster)
                zminras = min(zraster)
                zmaxras = max(zraster)
                if xminras > pix_width - 1 or xmaxras < 0 or zminras > pix_height - 1 or zmaxras < 0: 
                    continue
            
                # xmini = xraster.index(xminras)
                xmaxi = xraster.index(xmaxras)
                zmini = zraster.index(zminras)
                zmaxi = zraster.index(zmaxras)
                xminras = max(0, xminras)
                xmaxras = min(pix_width-1, xmaxras)
                zminras = max(0, zminras)
                zmaxras = min(pix_height-1, zmaxras)

                # Loop over pixels inside surrounding box arround triangle
                for i in range(int(round(xminras)),int(round(xmaxras))):
                    # Check pixels to the right
                    first_pix_found = False
                    jstart = int(round(min(max(1,zraster[xmaxi]),pix_width-1)))
                    j = jstart
                    while j <= int(round(zmaxras)):
                        pix = [i+0.5,j+0.5]
                        w[0] = edgeFunction(vs[-1],vs[0],pix)
                        w[1] = edgeFunction(vs[0],vs[1],pix)
                        w[2] = edgeFunction(vs[1],vs[2],pix)
                    
                        # Mark pixels inside triangle
                        if (w[0]<=0 and w[1]<=0 and w[2]<=0) or (w[0]>=0 and w[1]>=0 and w[2]>=0):
                            first_pix_found = True
                            p_ydist = findDepth(j+0.5, vs[zmini], vs[zmaxi])
                            if p_ydist < depthBuffer[j,i]:
                                image[j,i] = color*(max_dist-p_ydist)
                                depthBuffer[j,i] = p_ydist
                        elif first_pix_found:
                            break
                        j += 1
                
                    # Check pixels to the left
                    j = jstart-1
                    while j >= int(round(zminras)):
                        pix = [i+0.5,j+0.5]
                        w[0] = edgeFunction(vs[-1],vs[0],pix)
                        w[1] = edgeFunction(vs[0],vs[1],pix)
                        w[2] = edgeFunction(vs[1],vs[2],pix)

                        # Mark pixels inside triangle
                        if (w[0]<=0 and w[1]<=0 and w[2]<=0) or (w[0]>=0 and w[1]>=0 and w[2]>=0):
                            first_pix_found = True
                            p_ydist = findDepth(j+0.5, vs[zmini], vs[zmaxi] )
                            if p_ydist < depthBuffer[j,i]:
                                image[j,i] = color*(max_dist-p_ydist)
                                depthBuffer[j,i] = p_ydist
                        elif first_pix_found:
                            break
                        j -= 1
                    
    return image

