# Simulate stokes on mesh_air.xml and mesh_housebox.xml
# Here mesh_housebox.xml has mesh in the whole box inclusive the house
# The boundaries of the house are marked as 1 in mesh_housebox_facet_region.xml

from dolfin import *
from mshr import *
import numpy as np

class InflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.0)

class OutflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.0)

class NoslipBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not (near(x[0], 0.0) or near(x[0], 1.0))

if MPI.size(mpi_comm_world()) > 1:
    info("Sorry, this demo does not (yet) run in parallel.")
    exit(0)

# Create meshes
print 'Reading and building meshes...'
mesh_air = Mesh("meshes/mesh_air.xml")
mesh_housebox = Mesh("meshes/mesh_housebox.xml")
mesh_house = Mesh("meshes/mesh_house.xml")

xs = zip(*mesh_air.coordinates())
xmin = min(xs[0])
xmax = max(xs[0])
ymin = min(xs[1])
ymax = max(xs[1])
zmin = min(xs[2])
zmax = max(xs[2])
xd = xmax - xmin; yd = ymax - ymin; zd = zmax - zmin

#house_location = Point(xd/2+0.1,yd/2,zd/5)
#house_location = Point(xd/2+0.1,yd/2,zd/2)
house_location = Point(0.65*xd, 0.5*yd, 0.3*zd)
mesh_housebox.translate(house_location)
mesh_house.translate(house_location)

# Save the meshes to pvd-file
File("meshes/mesh_air.pvd") << mesh_air
File("meshes/mesh_housebox.pvd") << mesh_housebox
File("meshes/mesh_house.pvd") << mesh_house

# Build multimesh
multimesh = MultiMesh()
multimesh.add(mesh_air)
multimesh.add(mesh_housebox)
multimesh.build()

print 'Setting the scene...'
# Create function space
P2 = VectorElement("Lagrange", tetrahedron, 2)
P1 = FiniteElement("Lagrange", tetrahedron, 1)
TH = P2 * P1
W  = MultiMeshFunctionSpace(multimesh, TH)

# Define trial and test functions and right-hand side
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)
f = Constant((0, 0, 0))

# Define facet normal and mesh size
n = FacetNormal(multimesh)
h = 2.0*Circumradius(multimesh)

print 'Defining parameters and bilinear form'
# Parameters
alpha = 400.0

def tensor_jump(v, n):
    return outer(v('+'), n('+')) + outer(v('-'), n('-'))

def a_h(v, w):
    return inner(grad(v), grad(w))*dX \
         - inner(avg(grad(v)), tensor_jump(w, n))*dI \
         - inner(avg(grad(w)), tensor_jump(v, n))*dI \
         + alpha/avg(h) * inner(jump(v), jump(w))*dI

def b_h(v, q):
    return -div(v)*q*dX + jump(v, n)*avg(q)*dI

def l_h(v, q, f):
    return inner(f, v)*dX

def s_O(v, w):
    return inner(jump(grad(v)), jump(grad(w)))*dO

def s_C(v, q, w, r):
    return h*h*inner(-div(grad(v)) + grad(q), -div(grad(w)) - grad(r))*dC

def l_C(v, q, f):
    return h*h*inner(f, -div(grad(v)) - grad(q))*dC

# Define bilinear form
a = a_h(u, v) + b_h(v, p) + b_h(u, q) + s_O(u, v) + s_C(u, p, v, q)

print 'Defined bilinear form'

# Define linear form
L  = l_h(v, q, f) + l_C(v, q, f)

# Assemble linear system
A = assemble_multimesh(a)
b = assemble_multimesh(L)

# Create boundary values
in_exp = "sin((x[1] - ymin)*DOLFIN_PI/yd)*sin((x[2] - zmin)*DOLFIN_PI/zd)"
inflow_value = Expression((in_exp, "0.0", "0.0"), \
             ymin = ymin, zmin = zmin, yd = yd, zd = zd)
outflow_value = Constant(0)
noslip_value = Constant((0, 0, 0))

# Create subdomains for boundary conditions
inflow_boundary = InflowBoundary()
outflow_boundary = OutflowBoundary()
noslip_boundary = NoslipBoundary()

# Create subspaces for boundary conditions
V = MultiMeshSubSpace(W, 0)
Q = MultiMeshSubSpace(W, 1)

# Create boundary conditions
bc0 = MultiMeshDirichletBC(V, noslip_value,  noslip_boundary)
bc1 = MultiMeshDirichletBC(V, inflow_value,  inflow_boundary)
bc2 = MultiMeshDirichletBC(Q, outflow_value, outflow_boundary)

# Apply boundary conditions
bc0.apply(A, b)
bc1.apply(A, b)
bc2.apply(A, b)

# Find cells that collide with background mesh
cells_noslip = set(range(len(mesh_housebox.cells())))

tree_air = BoundingBoxTree()
tree_air.build(mesh_air)
tree_housebox = BoundingBoxTree()
tree_housebox.build(mesh_housebox)

cells_air, cells_housebox = tree_air.compute_entity_collisions(tree_housebox)
cells_housebox = set(cells_housebox)

# Remove cells that collide with background mesh
cells_noslip = cells_noslip.difference(cells_housebox)

# Find cells that collide with background boundary
boundary_mesh_air = BoundaryMesh(mesh_air, "exterior")
tree_boundary = BoundingBoxTree()
tree_boundary.build(boundary_mesh_air)

cells_boundary, cells_housebox = tree_boundary.compute_entity_collisions(tree_housebox)
cells_housebox = set(cells_housebox)

# Add cells that collide with background boundary
cells_noslip = cells_noslip.union(cells_housebox)

# Add house cells
house_markers = MeshFunction('size_t', mesh_housebox, 'meshes/mesh_housebox_physical_region.xml')
cells_house = set(np.where(house_markers.array() == 1)[0])
cells_noslip = cells_noslip.union(cells_house)
cells_noslip = cells_house

# Mark noslip facets
facet_markers = FacetFunction("size_t", mesh_housebox)
facet_markers.set_all(0)
for cell_index in cells_noslip:
    c = Cell(mesh_housebox, cell_index)
    for f in facets(c):
        facet_markers.set_value(f.index(), 1)

# Apply noslip to the house and sink domain
noslip_bc = MultiMeshDirichletBC(V, noslip_value, facet_markers, 1, 1)
noslip_bc.apply(A, b)

# Apply noslip to house boundary
#house_boundary_markers = MeshFunction('size_t', mesh_housebox, 'meshes/mesh_housebox_facet_region.xml')
#noslip_house_boundary = MultiMeshDirichletBC(V, noslip_value, house_boundary_markers, 1, 1)
#noslip_house_boundary.apply(A, b)


# Compute solution
w = MultiMeshFunction(W)
print 'Solving...'
solve(A, w.vector(), b)

# Extract solution components
u_air = w.part(0).sub(0)
u_hb = w.part(1).sub(0)
p_air = w.part(0).sub(1)
p_hb = w.part(1).sub(1)

# Save to file
File("solutions/u_air.pvd") << u_air
File("solutions/u_hb.pvd") << u_hb
File("solutions/p_air.pvd") << p_air
File("solutions/p_hb.pvd") << p_hb

File("solutions/facet_markers.pvd") << facet_markers 
File("solutions/house_markers.pvd") << house_markers 
#File("solutions/house_boundary_markers.pvd") << house_boundary_markers 


# Plot solution
#plot(mesh_air)
#plot(mesh_housebox)
#plot(u_air, title="u_air")
#plot(u_hb, title="u_hb")
#plot(p_air, title="p_air")
#plot(p_hb, title="p_hb")

#interactive()
