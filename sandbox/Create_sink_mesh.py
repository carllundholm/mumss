import os
from dolfin import *
import mshr
import numpy as np

# # Test mesh
# xmin = 0.0
# ymin = 0.0
# zmin = 0.0
# xmax = 0.5
# ymax = 0.5
# zmax = 0.5
# xy = [xmin,ymin,xmax,ymax]
# zbelow = -0.1 # Hight of sink mesh below
# surdom_island = mshr.Box(Point(xmin, ymin, zmin), Point(xmax, ymax, zmax))
# cone = mshr.Cone(Point(xmax/2, ymax/2, zmin), Point(xmax/2, ymax/2, zmax/3), ymax/2)
# mesh_island = mshr.generate_mesh(surdom_island - cone, 1)
# hmax = mesh_island.hmax()
#
# # Save the mesh
# File("meshes/mesh_island.xml") << mesh_island
# File("meshes/mesh_island.pvd") << mesh_island


# Island mesh
directory = "meshes"
mesh_island = Mesh(os.path.join(directory, "mesh_island.xml"))
hmax = mesh_island.hmax()
xs = zip(*mesh_island.coordinates())
xmin = min(xs[0])
xmax = max(xs[0])
ymin = min(xs[1])
ymax = max(xs[1])
zmin = min(xs[2])
zmax = max(xs[2])

xy = [xmin,ymin,xmax,ymax]
zbelow = -(zmax-zmin)*0.2 #-0.1 # Hight of sink mesh below


# Make neighbouring mesh
file = open(os.path.join(directory, 'neighbouring_mesh.geo'),'w') # Create .geo file
file.write('cl1 = %f; \n' % 1.0)

# Find coordinats and dofs on the common boundary surface
class IslandDomain(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not( (near(x[0],xmin) or near(x[0],xmax) \
                or near(x[1],ymin,DOLFIN_EPS*10) or near(x[1],ymax)  or near(x[2],zmax)) and not near(x[2],zmin))

island = IslandDomain()
V = FunctionSpace(mesh_island, 'CG', 1)
bc0 = DirichletBC(V, Constant(1.0), island)
u = Function(V)
bc0.apply(u.vector())
gdim = mesh_island.geometry().dim()
_all_coor = V.dofmap().tabulate_all_coordinates(mesh_island).reshape((-1,gdim))
_coor = _all_coor[u.vector() == 1.0]
_dofs = np.where(u.vector() == 1.0)[0]


# Write points to .geo file
print 'Writing points to .geo fil'
xypoints = [[] for _ in range(4)]   # holdes [dofs_near_xmin,dofs_near_ymin,dofs_near_xmax,dofs_near_ymax]
limits = 4*[[]]                     # holdes the four limit points
for i in range(len(_dofs)):
    file.write('Point(%d) = {%.10f, %.10f, %.10f, cl1}; \n' % (_dofs[i],_coor[i][0],_coor[i][1],_coor[i][2]))
    if near(_coor[i][0], xmin,DOLFIN_EPS*10):
        if near(_coor[i][1],ymin,DOLFIN_EPS*10):
            limits[0] = _dofs[i] #xmin_ymin_point
        elif near(_coor[i][1], ymax,DOLFIN_EPS*10):
            limits[1] = _dofs[i] #xmin_ymax_point
    elif near(_coor[i][0], xmax,DOLFIN_EPS*10):
        if near(_coor[i][1],ymax,DOLFIN_EPS*10):
            limits[2] = _dofs[i] #max_ymax_point
        elif near(_coor[i][1],ymin,DOLFIN_EPS*10):
            limits[3] = _dofs[i] #xmax_ymin_point
    for j in range(4): 
        if near(_coor[i][j%2],xy[j],DOLFIN_EPS*10): 
            xypoints[j].append(_dofs[i])


# Search in lines for a line between point1 and point2
def Does_Line_Exist(point1, point2, lines):
    # returns: [bool exists, int directed_linenumber]
    try:
        _lines = lines[point1]
        l = len(_lines)
    except IndexError:
        return [False, 'nan']
    for j in range(l):
        if _lines[j][0] == point2:
            return [True, _lines[j][1]]
    return [False, 'nan']

        

# Create lines, lineloop, and surfaces in .geo
# Find facet functions on common boundary
print 'Creates boundary surface'
ff = FacetFunction('size_t', mesh_island)
island.mark(ff, 1)
vd = vertex_to_dof_map(V)
count_lines = 1
count_surf = 1
num_dofs = _dofs[-1]+1 #len(V.dofmap().dofs())+1
lines = [[] for _ in range(num_dofs+4)]
for f in facets(mesh_island):
    if ff[f]==1:
        # FIXME only works if there is three dofs in one facet
        dofs_on_facet = vd[f.entities(0)]; l_no = [[]]*3 
        for i in range(0,3):
            [exits, l_no[i]] = Does_Line_Exist(dofs_on_facet[i-1], dofs_on_facet[i], lines)
            if not exits:
                lines[dofs_on_facet[i-1]].append([dofs_on_facet[i],count_lines])
                lines[dofs_on_facet[i]].append([dofs_on_facet[i-1],-count_lines])
                file.write('Line(%d) = {%d, %d}; \n' % (count_lines,dofs_on_facet[i-1],dofs_on_facet[i]))
                l_no[i] = count_lines
                count_lines += 1         
        file.write('Line Loop(%d) = {%d, %d, %d}; \n' % (count_surf,l_no[0],l_no[1],l_no[2]))
        file.write('Plane Surface(%d) = {%d}; \n' % (count_surf,count_surf))
        count_surf += 1

#
# Make box below
#


# Make buttom points
print 'Make mesh below island'
count = 0
for [i,j] in [[0,1],[0,3],[2,3],[2,1]]:
    file.write('Point(%d) = {%f, %f, %f, cl1}; \n' % (num_dofs+count,xy[i],xy[j],zbelow))
    xypoints[i].append(num_dofs+count)
    xypoints[j].append(num_dofs+count)
    count += 1

# Connect buttom points with lines
for i in range(4):
    lines[num_dofs+i].append([num_dofs+(i+1)%4,count_lines+i])
    lines[num_dofs+(i+1)%4].append([num_dofs+i,-(count_lines+i)])
    file.write('Line(%d) = {%d, %d}; \n' % (count_lines+i,num_dofs+i,num_dofs+(i+1)%4))
    lines[num_dofs+i].append([limits[i],count_lines+i+4])
    lines[limits[i]].append([num_dofs+i,-(count_lines+i+4)])
    file.write('Line(%d) = {%d, %d}; \n' % (count_lines+i+4,num_dofs+i,limits[i]))

# Make surface at xmin, xmax, ymin and ymax
for k in range(4):
    first_point = xypoints[k][0]; point1 = first_point
    temp = np.delete(xypoints[k],0) # List of remaining points in surface
    file.write('Line Loop(%d) = {' % count_surf)
    for j in range(len(temp)):
        exist = False; i = 0
        while not exist:    # Search for neighbouring point in surface
            point2 = temp[i]
            [exist, l_no] = Does_Line_Exist(point1,point2,lines)
            i += 1
        file.write('%d,' % l_no)
        point1 = point2
        temp = np.delete(temp,i-1)
    [exist, l_no] = Does_Line_Exist(point1,first_point,lines)
    file.write('%d}; \n' % l_no)
    file.write('Plane Surface(%d) = {%d}; \n' % (count_surf,count_surf))
    count_surf += 1

# Make buttom surface:
file.write('Line Loop(%d) = {%d, %d, %d, %d}; \n' % (count_surf, count_lines, count_lines+1, count_lines+2, count_lines+3))
file.write('Plane Surface(%d) = {%d}; \n' % (count_surf,count_surf))

# Make surface loop:
file.write('Surface Loop(%d) = {' % (count_surf+1))
for i in range(count_surf-1):
    file.write('%d, ' % (i+1))
file.write('%d}; \n' % count_surf)

# Make volume, physical surface and volume
file.write('Volume(%d) = {%d}; \n' % (count_surf+2,count_surf+1))
file.write('Physical Surface(%d) = {%d}; \n' % (count_surf+3,count_surf+1))
file.write('Physical Volume(%d) = {%d}; \n' % (count_surf+4,count_surf+2))

file.close()

# Build mesh
os.chdir(directory)
# FIXME you need to have gmsh installed in /Applications/Gmsh/
os.system("/Applications/Gmsh/Gmsh.app/Contents/MacOS/gmsh neighbouring_mesh.geo -3 mesh.msh")
os.remove("neighbouring_mesh.geo")
os.system("python dolfin-convert.py neighbouring_mesh.msh neighbouring_mesh.xml")
os.remove("neighbouring_mesh.msh")