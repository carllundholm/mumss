from fenics import *
from dolfin import *
from mshr import *
import numpy as np

xmin = 0.0; xmax = 1.0; ymin = 0.0; ymax = 1.0
xd = xmax - xmin; yd = ymax - ymin

class InflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], xmin)

class OutflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], xmax)

class NoslipBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not (near(x[0], xmin) or near(x[0], xmax))

if MPI.size(mpi_comm_world()) > 1:
    info("Sorry, this demo does not (yet) run in parallel.")
    exit(0)

# Create meshes
mesh_air = UnitSquareMesh(4, 4)

hb_geo = Rectangle(Point(0.0, 0.0), Point(0.4, 0.4))
h_geo = Rectangle(Point(0.1, 0.1), Point(0.3, 0.3))
hb_geo.set_subdomain(1, h_geo)

mesh_housebox = generate_mesh(hb_geo, 10)
housebox_location = Point(0.3, -0.2)
mesh_housebox.translate(housebox_location)

# Save the meshes to pvd-file
File("meshes/mesh_air.pvd") << mesh_air
File("meshes/mesh_housebox.pvd") << mesh_housebox

# Build multimesh
multimesh = MultiMesh()
multimesh.add(mesh_air)
multimesh.add(mesh_housebox)
multimesh.build()

print 'Setting the scene...'
# Create function space
P2 = VectorElement("P", triangle, 2)
P1 = FiniteElement("P", triangle, 1)
TH = P2 * P1
W  = MultiMeshFunctionSpace(multimesh, TH)

# Define trial and test functions and right-hand side
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)
f = Constant((0, 0))

# Define facet normal and mesh size
n = FacetNormal(multimesh)
h = 2.0*Circumradius(multimesh)

print 'Defining parameters and bilinear form'
# Parameters
alpha = 400.0

def tensor_jump(v, n):
    return outer(v('+'), n('+')) + outer(v('-'), n('-'))

def a_h(v, w):
    return inner(grad(v), grad(w))*dX \
         - inner(avg(grad(v)), tensor_jump(w, n))*dI \
         - inner(avg(grad(w)), tensor_jump(v, n))*dI \
         + alpha/avg(h) * inner(jump(v), jump(w))*dI

def b_h(v, q):
    return -div(v)*q*dX + jump(v, n)*avg(q)*dI

def l_h(v, q, f):
    return inner(f, v)*dX

def s_O(v, w):
    return inner(jump(grad(v)), jump(grad(w)))*dO

def s_C(v, q, w, r):
    return h*h*inner(-div(grad(v)) + grad(q), -div(grad(w)) - grad(r))*dC

def l_C(v, q, f):
    return h*h*inner(f, -div(grad(v)) - grad(q))*dC

# Define bilinear form
a = a_h(u, v) + b_h(v, p) + b_h(u, q) + s_O(u, v) + s_C(u, p, v, q)

print 'Defined bilinear form'

# Define linear form
L  = l_h(v, q, f) + l_C(v, q, f)

# Assemble linear system
A = assemble_multimesh(a)
b = assemble_multimesh(L)

# Create boundary values
in_exp = "sin((x[1] - ymin)*DOLFIN_PI/yd)"
inflow_value = Expression((in_exp, "0.0"), ymin = ymin, yd = yd)
outflow_value = Constant(0)
noslip_value = Constant((0, 0))

# Create subdomains for boundary conditions
inflow_boundary = InflowBoundary()
outflow_boundary = OutflowBoundary()
noslip_boundary = NoslipBoundary()

# Create subspaces for boundary conditions
V = MultiMeshSubSpace(W, 0)
Q = MultiMeshSubSpace(W, 1)

# Create boundary conditions
bc0 = MultiMeshDirichletBC(V, noslip_value,  noslip_boundary)
bc1 = MultiMeshDirichletBC(V, inflow_value,  inflow_boundary)
bc2 = MultiMeshDirichletBC(Q, outflow_value, outflow_boundary)

# Apply boundary conditions
bc0.apply(A, b)
bc1.apply(A, b)
bc2.apply(A, b)

# Find cells that collide with background mesh
cells_noslip = set(range(len(mesh_housebox.cells())))

tree_air = BoundingBoxTree()
tree_air.build(mesh_air)
tree_housebox = BoundingBoxTree()
tree_housebox.build(mesh_housebox)

cells_air, cells_housebox = tree_air.compute_entity_collisions(tree_housebox)
cells_housebox = set(cells_housebox)

# Remove cells that collide with background mesh
cells_noslip = cells_noslip.difference(cells_housebox)

# Find cells that collide with background boundary
boundary_mesh_air = BoundaryMesh(mesh_air, "exterior")
tree_boundary = BoundingBoxTree()
tree_boundary.build(boundary_mesh_air)

cells_boundary, cells_housebox = tree_boundary.compute_entity_collisions(tree_housebox)
cells_housebox = set(cells_housebox)

# Add cells that collide with background boundary
cells_noslip = cells_noslip.union(cells_housebox)

# Add house cells
house_markers = MeshFunction('size_t', mesh_housebox, 2, mesh_housebox.domains())
cells_house = set(np.where(house_markers.array() == 1)[0])
cells_noslip = cells_noslip.union(cells_house)
# cells_noslip = cells_house

# Mark noslip facets
facet_markers = FacetFunction("size_t", mesh_housebox)
facet_markers.set_all(0)
for cell_index in cells_noslip:
    c = Cell(mesh_housebox, cell_index)
    for f in facets(c):
        facet_markers.set_value(f.index(), 1)

# Apply noslip to the house and sink domain
noslip_bc = MultiMeshDirichletBC(V, noslip_value, facet_markers, 1, 1)
noslip_bc.apply(A, b)

# Compute solution
w = MultiMeshFunction(W)
print 'Solving...'
solve(A, w.vector(), b)

# Extract solution components
u_air = w.part(0).sub(0)
u_hb = w.part(1).sub(0)
p_air = w.part(0).sub(1)
p_hb = w.part(1).sub(1)

# Save to file
File("solutions/u_air.pvd") << u_air
File("solutions/u_hb.pvd") << u_hb
File("solutions/p_air.pvd") << p_air
File("solutions/p_hb.pvd") << p_hb

File("solutions/facet_markers.pvd") << facet_markers
File("solutions/house_markers.pvd") << house_markers
#File("solutions/house_boundary_markers.pvd") << house_boundary_markers


# Plot solution
#plot(mesh_air)
#plot(mesh_housebox)
#plot(u_air, title="u_air")
#plot(u_hb, title="u_hb")
#plot(p_air, title="p_air")
#plot(p_hb, title="p_hb")

#interactive()
