from mshr import *
from dolfin import *
import numpy

npa = numpy.array

stlfile = "island_enkel-A.stl"
g = Surface3D(stlfile)

# Create a mesh around the island
d = CSGCGALDomain3D(g)
v = d.get_vertices()

lenv = len(v)
vx = npa([v[i][0] for i in range(0, lenv)])
vy = npa([v[i][1] for i in range(0, lenv)])
vz = npa([v[i][2] for i in range(0, lenv)])

xmin = min(vx); xmax = max(vx)
ymin = min(vy); ymax = max(vy)
zmin = min(vz); zmax = max(vz)

il = xmax - xmin
iw = ymax - ymin
ih = zmax - zmin

bxmin = xmin - 0.1*il; bxmax = xmax + 0.1*il
bymin = ymin - 0.1*iw; bymax = ymax + 0.1*iw;
bzmin = 1.0*zmin; bzmax = zmax + 2.0*ih

surdom = Box(Point(bxmin, bymin, bzmin), Point(bxmax, bymax, bzmax))

mesh_island = generate_mesh(surdom - g, 10)

# Locate the highest point on the island
for i in range(0, lenv):
    if vz[i] == zmax:
	xhp = vx[i]
	yhp = vy[i]

highest_point = Point(xhp, yhp, zmax)

# Normalize the mesh
xs = mesh_island.coordinates()
xmin = min([x[0] for x in xs])
xmax = max([x[0] for x in xs])
ymin = min([x[1] for x in xs])
ymax = max([x[1] for x in xs])
zmin = min([x[2] for x in xs])
zmax = max([x[2] for x in xs])

min_coord_i = npa([xmin, ymin, zmin])
dist = max(xmax - xmin, ymax - ymin, zmax - zmin)

for i, x in enumerate(xs):
    xs[i][0] = (x[0] - xmin) / dist
    xs[i][1] = (x[1] - ymin) / dist
    xs[i][2] = (x[2] - zmin) / dist

# Save the mesh
File("meshes/mesh_island.xml") << mesh_island
File("meshes/mesh_island.pvd") << mesh_island  
