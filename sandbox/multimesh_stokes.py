from dolfin import *

degree = 3

set_log_level(PROGRESS)

# Load meshes
mesh_0 = Mesh("meshes/mesh_island.xml")
mesh_1 = Mesh("meshes/mesh_house.xml")

xs = mesh_0.coordinates()
xmin = min([x[0] for x in xs])
xmax = max([x[0] for x in xs])
ymin = min([x[1] for x in xs])
ymax = max([x[1] for x in xs])
zmin = min([x[2] for x in xs])
zmax = max([x[2] for x in xs])

xd = xmax - xmin; yd = ymax - ymin; zd = zmax - zmin 

xsh = mesh_1.coordinates()
xminh = min([x[0] for x in xsh])
xmaxh = max([x[0] for x in xsh])
yminh = min([x[1] for x in xsh])
ymaxh = max([x[1] for x in xsh])
zminh = min([x[2] for x in xsh])
zmaxh = max([x[2] for x in xsh])

xdh = xmaxh - xminh; ydh = ymaxh - yminh; zdh = zmaxh -zminh
xep = 0.1*xdh; yep = 0.1*ydh; zep = 0.1*zdh

class InflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], xminh) #\
	   # and not (near(x[1], ymin) or near(x[1], ymax) \
	   #	  or near(x[2], zmin) or near(x[2], zmax) )
    
class OutflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], xmaxh) #\
	   # and not (near(x[1], ymin) or near(x[1], ymax) \
		#  or near(x[2], zmin) or near(x[2], zmax) )

class NoslipBoundary(SubDomain):
    def inside(self, x, on_boundary):
	check = on_boundary and not (near(x[0], xminh) or near(x[0], xmaxh)) \
	#		         or  (xminh - xep < x[0] and x[0] < xminh + xep \
	#			  and yminh - yep < x[1] and x[1] < ymaxh + yep \
	#			  and zminh - zep < x[2] and x[2] < zmaxh + zep) \
	#		 	 or  (xmaxh - xep < x[0] and x[0] < xmaxh + xep \
	#			  and yminh - yep < x[1] and x[1] < ymaxh + yep \
	#			  and zminh - zep < x[2] and x[2] < zmaxh + zep) \
	#			 or  (yminh - yep < x[1] and x[1] < yminh + yep \
	#			  and xminh - xep < x[0] and x[0] < xmaxh + yep \
	#			  and zminh - zep < x[2] and x[2] < zmaxh + zep) \
	#		 	 or  (ymaxh - yep < x[1] and x[1] < ymaxh + yep \
	#			  and xminh - xep < x[0] and x[0] < xmaxh + xep \
	#			  and zminh - zep < x[2] and x[2] < zmaxh + zep) \
	#			 or  (zminh - zep < x[2] and x[2] < zminh + zep \
	#			  and yminh - yep < x[1] and x[1] < ymaxh + yep \
	#			  and xminh - xep < x[0] and x[0] < xmaxh + xep) \
	#		 	 or  (zmaxh - zep < x[2] and x[2] < zmaxh + zep \
	#			  and yminh - yep < x[1] and x[1] < ymaxh + yep \
	#			  and xminh - xep < x[0] and x[0] < xmaxh + xep))
	 
	#check = on_boundary and not (near(x[0], xmin) or near(x[0], xmax)) #\
			    #and not (near(x[1], yminh) or near(x[1], ymaxh) \
				#  or near(x[2], zminh) or near(x[2], zmaxh)))
        #return on_boundary and x[0] > xminh + xep and x[0] < xmaxh - xep \
	#		   and x[1] > yminh + yep and x[1] < ymaxh - yep \
	#		   and x[2] > zminh + zep and x[2] < zmaxh - zep 

	#print "Noslip:", x, check

	return check	
         
if MPI.size(mpi_comm_world()) > 1:
    info("Sorry, this demo does not (yet) run in parallel.")
    exit(0)

# Build multimesh
multimesh = MultiMesh()
#multimesh.add(mesh_0)
multimesh.add(mesh_1)
multimesh.build(1)

# Create function space
P2 = VectorElement("Lagrange", tetrahedron, 2)
P1 = FiniteElement("Lagrange", tetrahedron, 1)
TH = P2 * P1
W  = MultiMeshFunctionSpace(multimesh, TH)

# Define trial and test functions and right-hand side

f = Constant((0, 0, 0))

# Define trial and test functions and right-hand side
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)

# Define facet normal and mesh size
n = FacetNormal(multimesh)
h = 2.0*Circumradius(multimesh)

# Parameters
alpha = 4.0

def tensor_jump(v, n):
    return outer(v('+'), n('+')) + outer(v('-'), n('-'))

def a_h(v, w):
    return inner(grad(v), grad(w))*dX \
         - inner(avg(grad(v)), tensor_jump(w, n))*dI \
         - inner(avg(grad(w)), tensor_jump(v, n))*dI \
         + alpha/avg(h) * inner(jump(v), jump(w))*dI

def b_h(v, q):
    return -div(v)*q*dX + jump(v, n)*avg(q)*dI

def l_h(v, q, f):
    return inner(f, v)*dX

def s_O(v, w):
    return inner(jump(grad(v)), jump(grad(w)))*dO

def s_C(v, q, w, r):
    return h*h*inner(-div(grad(v)) + grad(q), -div(grad(w)) - grad(r))*dC

def l_C(v, q, f):
    return h*h*inner(f, -div(grad(v)) - grad(q))*dC

# Define bilinear form
a = a_h(u, v) + b_h(v, p) + b_h(u, q) + s_O(u, v) + s_C(u, p, v, q)

# Define linear form
L  = l_h(v, q, f) + l_C(v, q, f)

# Assemble linear system
A = assemble_multimesh(a)
b = assemble_multimesh(L)

# Create boundary values
in_exp = "xd*sin((x[1] - ymin)*DOLFIN_PI/yd)*sin((x[2] - zmin)*DOLFIN_PI/zd)"
#inflow_value = Expression((in_exp, "0.0", "0.0"), \
#			 ymin = ymin, zmin = zmin, xd = 1.0, yd = yd, zd = zd)
inflow_value = Expression((in_exp, "0.0", "0.0"), \
			 ymin = yminh, zmin = zminh, xd = xdh, yd = ydh, zd = zdh)
outflow_value = Constant(0)
noslip_value = Constant((0, 0, 0))
#noslip_value = Expression(("0.0", "0.0", "0.0"))

# Create subdomains for boundary conditions
inflow_boundary = InflowBoundary()
outflow_boundary = OutflowBoundary()
noslip_boundary = NoslipBoundary()

# Create subspaces for boundary conditions
V = MultiMeshSubSpace(W, 0)
Q = MultiMeshSubSpace(W, 1)

# Create boundary conditions
bc0 = MultiMeshDirichletBC(V, noslip_value,  noslip_boundary)
bc1 = MultiMeshDirichletBC(V, inflow_value,  inflow_boundary)
bc2 = MultiMeshDirichletBC(Q, outflow_value, outflow_boundary)

# Apply boundary conditions
bc0.apply(A, b)
bc1.apply(A, b)
bc2.apply(A, b)

# Compute solution
w = MultiMeshFunction(W)
solve(A, w.vector(), b)

# Extract solution components
u0 = w.part(0).sub(0)
#u1 = w.part(1).sub(0)
p0 = w.part(0).sub(1)
#p1 = w.part(1).sub(1)

# Save to file
File("solutions/u_house_2015-12-15.pvd") << u0
#File("solutions/u1_2015-12-15.pvd") << u1
File("solutions/p_house_2015-12-15.pvd") << p0
#File("solutions/p1_2015-12-15.pvd") << p1

# Plot solution
#plot(W.multimesh(), interactive=True)
#plot(u0, title="u_0", interactive=True)
#plot(u1, title="u_1", interactive=True)
#plot(p0, title="p_0", interactive=True)
#plot(p1, title="p_1", interactive=True)
#interactive()
