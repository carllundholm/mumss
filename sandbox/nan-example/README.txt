I run meshgenerator.py with FEniCS v1.6.0 as the mshr in my development version doesn't produce nice meshes.

Run meshgenerator.py
Run multimesh_stokes_on_box_mesh.py

Observe the pressure results is fine.
If you inside meshgenerator.py i line 18 change the z coordinate to -0.025 the pressure solution will produce nan's in the four corners which peep out. 