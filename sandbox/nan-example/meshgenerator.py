from dolfin import *
import mshr

# Big box mesh
box = mshr.Box(Point(0.0, 0.0, 0.0), Point(1.0, 1.0, 1.0))
mesh_unit_box = mshr.generate_mesh(box, 10)

# Save the mesh
File("meshes/mesh_unit_box.xml") << mesh_unit_box
File("meshes/mesh_unit_box.pvd") << mesh_unit_box  

# Small box mesh
box = mshr.Box(Point(0, 0, 0), Point(0.1, 0.1, 0.1))
box1 = mshr.Box(Point(0.04, 0.04, 0.04), Point(0.06, 0.06, 0.06))
mesh_small_box = mshr.generate_mesh(box-box1, 5)

# Move the house to a location on the island
location = Point(0.45, 0.45, -0.024)    #z-coordinat -0.024 works, but -0.025 produces nan's in pressure solution
mesh_small_box.translate(location)

# Save the mesh
File("meshes/mesh_small_box.xml") << mesh_small_box
File("meshes/mesh_small_box.pvd") << mesh_small_box 