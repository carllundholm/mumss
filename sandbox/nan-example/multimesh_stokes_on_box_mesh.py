from dolfin import *

set_log_level(PROGRESS)

# Load meshes
mesh_0 = Mesh("meshes/mesh_unit_box.xml")
mesh_1 = Mesh("meshes/mesh_small_box.xml")

class InflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0)
    
class OutflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1)

class NoslipBoundary(SubDomain):
    def inside(self, x, on_boundary):
	return on_boundary and not (near(x[0], 0) or near(x[0], 1))	

# Build multimesh
multimesh = MultiMesh()
multimesh.add(mesh_0)
multimesh.add(mesh_1)
multimesh.build(1)

# Create function space
P2 = VectorElement("Lagrange", tetrahedron, 2)
P1 = FiniteElement("Lagrange", tetrahedron, 1)
TH = P2 * P1
W  = MultiMeshFunctionSpace(multimesh, TH)


# Define trial and test functions and right-hand side
f = Constant((0, 0, 0))

# Define trial and test functions and right-hand side
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)

# Define facet normal and mesh size
n = FacetNormal(multimesh)
h = 2.0*Circumradius(multimesh)

# Parameters
alpha = 400.0

def tensor_jump(v, n):
    return outer(v('+'), n('+')) + outer(v('-'), n('-'))

def a_h(v, w):
    return inner(grad(v), grad(w))*dX \
         - inner(avg(grad(v)), tensor_jump(w, n))*dI \
         - inner(avg(grad(w)), tensor_jump(v, n))*dI \
         + alpha/avg(h) * inner(jump(v), jump(w))*dI

def b_h(v, q):
    return -div(v)*q*dX + jump(v, n)*avg(q)*dI

def l_h(v, q, f):
    return inner(f, v)*dX

def s_O(v, w):
    return inner(jump(grad(v)), jump(grad(w)))*dO

def s_C(v, q, w, r):
    return h*h*inner(-div(grad(v)) + grad(q), -div(grad(w)) - grad(r))*dC 

def l_C(v, q, f):
    return h*h*inner(f, -div(grad(v)) - grad(q))*dC

# Define bilinear form
a = a_h(u, v) + b_h(v, p) + b_h(u, q) + s_O(u, v) + s_C(u, p, v, q)

# Define linear form
L  = l_h(v, q, f) + l_C(v, q, f)

# Assemble linear system
A = assemble_multimesh(a)
b = assemble_multimesh(L)

# Create boundary values
inflow_value = Expression(("sin(x[1]*DOLFIN_PI)*sin(x[2]*DOLFIN_PI)", "0.0", "0.0"))
outflow_value = Constant(0)
noslip_value = Constant((0, 0, 0))

# Create subdomains for boundary conditions
inflow_boundary = InflowBoundary()
outflow_boundary = OutflowBoundary()
noslip_boundary = NoslipBoundary()

# Create subspaces for boundary conditions
V = MultiMeshSubSpace(W, 0)
Q = MultiMeshSubSpace(W, 1)

# Create boundary conditions
bc0 = MultiMeshDirichletBC(V, noslip_value,  noslip_boundary)
bc1 = MultiMeshDirichletBC(V, inflow_value,  inflow_boundary)
bc2 = MultiMeshDirichletBC(Q, outflow_value, outflow_boundary)

# Apply boundary conditions
bc0.apply(A, b)
bc1.apply(A, b)
bc2.apply(A, b)

# Compute solution
w = MultiMeshFunction(W)
solve(A, w.vector(), b)

# Extract solution components
u0 = w.part(0).sub(0)
u1 = w.part(1).sub(0)
p0 = w.part(0).sub(1)
p1 = w.part(1).sub(1)

# Save to file
File("solutions/u0_unit_box.pvd") << u0
File("solutions/u1_small_box.pvd") << u1
File("solutions/p0_unit_box.pvd") << p0
File("solutions/p1_small_box.pvd") << p1

# Plot solution
# plot(W.multimesh(), interactive=True)
plot(u0, title="u_0")
plot(u1, title="u_1")
plot(p0, title="p_0")
plot(p1, title="p_1")
interactive()
