from dolfin import *
from mshr import *

class InflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.0)

class OutflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.0)

class NoslipBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not (near(x[0], 0.0) or near(x[0], 1.0))

class SinkHouse(SubDomain):
    def inside(self, x, on_boundary):
	return (between(x[0], (0.3, 0.7)) and between(x[1], (-0.25, 0.25)))

class SinkGround(SubDomain):
    def inside(self, x, on_boundary):
	return (between(x[0], (0.0, 1.0)) and between(x[1], (-0.5, 0.0)))  

if MPI.size(mpi_comm_world()) > 1:
    info("Sorry, this demo does not (yet) run in parallel.")
    exit(0)

geo_air = Rectangle(Point(0.0, 0.0), Point(1.0, 1.0))
geo_house =  Rectangle(Point(0.3, -0.25), Point(0.7, 0.25))
geo_housebox = Rectangle(Point(0.1, -0.5), Point(0.9, 0.5)) - geo_house
geo_ground = Rectangle(Point(0.0, -0.5), Point(1.0, 0.0))

# Create meshes
mesh_air = generate_mesh(geo_air, 15)
mesh_housebox = generate_mesh(geo_housebox, 10)
sinkmesh_house = generate_mesh(geo_house, 1)
sinkmesh_ground = generate_mesh(geo_ground, 1) #Goes wrong when meshsize = 10

# Build multimesh
multimesh = MultiMesh()
multimesh.add(mesh_air)
multimesh.add(mesh_housebox)
multimesh.add(sinkmesh_house)
multimesh.add(sinkmesh_ground)
multimesh.build()

# Create function space
P2 = VectorElement("Lagrange", triangle, 2)
P1 = FiniteElement("Lagrange", triangle, 1)
TH = P2 * P1
W  = MultiMeshFunctionSpace(multimesh, TH)

# Define trial and test functions and right-hand side
u = TrialFunction(W)
v = TestFunction(W)
f = Constant((0, 0))

# Define trial and test functions and right-hand side
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)

# Define facet normal and mesh size
n = FacetNormal(multimesh)
h = 2.0*Circumradius(multimesh)

# Parameters
alpha = 4.0

def tensor_jump(v, n):
    return outer(v('+'), n('+')) + outer(v('-'), n('-'))

def a_h(v, w):
    return inner(grad(v), grad(w))*dX \
         - inner(avg(grad(v)), tensor_jump(w, n))*dI \
         - inner(avg(grad(w)), tensor_jump(v, n))*dI \
         + alpha/avg(h) * inner(jump(v), jump(w))*dI

def b_h(v, q):
    return -div(v)*q*dX + jump(v, n)*avg(q)*dI

def l_h(v, q, f):
    return inner(f, v)*dX

def s_O(v, w):
    return inner(jump(grad(v)), jump(grad(w)))*dO

def s_C(v, q, w, r):
    return h*h*inner(-div(grad(v)) + grad(q), -div(grad(w)) - grad(r))*dC

def l_C(v, q, f):
    return h*h*inner(f, -div(grad(v)) - grad(q))*dC

# Define bilinear form
a = a_h(u, v) + b_h(v, p) + b_h(u, q) + s_O(u, v) + s_C(u, p, v, q)

# Define linear form
L  = l_h(v, q, f) + l_C(v, q, f)

# Assemble linear system
A = assemble_multimesh(a)
b = assemble_multimesh(L)

# Create boundary values
inflow_value = Expression(("sin(x[1]*DOLFIN_PI)", "0.0"))
outflow_value = Constant(0)
noslip_value = Constant((0, 0))

# Create subdomains for boundary conditions
inflow_boundary = InflowBoundary()
outflow_boundary = OutflowBoundary()
noslip_boundary = NoslipBoundary()

# Create subspaces for boundary conditions
V = MultiMeshSubSpace(W, 0)
Q = MultiMeshSubSpace(W, 1)

# Create boundary conditions
bc0 = MultiMeshDirichletBC(V, noslip_value,  noslip_boundary)
bc1 = MultiMeshDirichletBC(V, inflow_value,  inflow_boundary)
bc2 = MultiMeshDirichletBC(Q, outflow_value, outflow_boundary)

# Apply boundary conditions
bc0.apply(A, b)
bc1.apply(A, b)
bc2.apply(A, b)

# Create subdomains for sink domains
sink_house = SinkHouse()
sink_ground = SinkGround()  

# Create sink domains
sd_house = MultiMeshDirichletBC(V, noslip_value, sink_house)
sd_ground = MultiMeshDirichletBC(V, noslip_value, sink_ground)

# Apply sink domains 
sd_house.apply(A, b)
sd_ground.apply(A, b)

# Compute solution
w = MultiMeshFunction(W)
solve(A, w.vector(), b)

# FIXME: w.part(i).split() not working for extracted parts
# FIXME: since they are only dolfin::Functions

# Extract solution components
u_air = w.part(0).sub(0)
u_hb = w.part(1).sub(0)
u_sh = w.part(2).sub(0)
u_sg = w.part(3).sub(0)
p_air = w.part(0).sub(1)
p_hb = w.part(1).sub(1)
p_sh = w.part(2).sub(1)
p_sg = w.part(3).sub(1)

# Save to file
File("solutions_sinkmesh/u_air.pvd") << u_air
File("solutions_sinkmesh/u_hb.pvd") << u_hb
File("solutions_sinkmesh/u_sh.pvd") << u_sh
File("solutions_sinkmesh/u_sg.pvd") << u_sg
File("solutions_sinkmesh/p_air.pvd") << p_air
File("solutions_sinkmesh/p_hb.pvd") << p_hb
File("solutions_sinkmesh/p_sh.pvd") << p_sh
File("solutions_sinkmesh/p_sg.pvd") << p_sg

# Plot solution
# plot(W.multimesh())
#plot(mesh_0)
plot(u_air, title="u_air", interactive=True)
plot(u_hb, title="u_hb", interactive=True)
#plot(p0, title="p_0")
#plot(p1, title="p_1")
#interactive()
