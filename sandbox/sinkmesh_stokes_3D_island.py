from dolfin import *
from mshr import *

class InflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.0)

class OutflowBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.0)

class NoslipBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not (near(x[0], 0.0) or near(x[0], 1.0))

class SinkGround(SubDomain):
    def inside(self, x, on_boundary):
        return sink_ground_tree.collides(Point(x[0],x[1],x[2]))

if MPI.size(mpi_comm_world()) > 1:
    info("Sorry, this demo does not (yet) run in parallel.")
    exit(0)

# Create meshes
mesh_air = Mesh("meshes/mesh_island.xml")
mesh_housebox = Mesh("meshes/mesh_house.xml")
sinkmesh_ground = Mesh("meshes/neighbouring_mesh.xml")
# File("neighbouring_mesh.pvd") << sinkmesh_ground

xs = zip(*mesh_air.coordinates())
xmin = min(xs[0])
xmax = max(xs[0])
ymin = min(xs[1])
ymax = max(xs[1])
zmin = min(xs[2])
zmax = max(xs[2])
xd = xmax - xmin; yd = ymax - ymin; zd = zmax - zmin 

# Build multimesh
multimesh = MultiMesh()
multimesh.add(mesh_air)
# multimesh.add(mesh_housebox)
multimesh.add(sinkmesh_ground)
multimesh.build()

# Create function space
P2 = VectorElement("Lagrange", tetrahedron, 2)
P1 = FiniteElement("Lagrange", tetrahedron, 1)
TH = P2 * P1
W  = MultiMeshFunctionSpace(multimesh, TH)

# Define trial and test functions and right-hand side
f = Constant((0, 0, 0))

# Define trial and test functions and right-hand side
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)

# Define facet normal and mesh size
n = FacetNormal(multimesh)
h = 2.0*Circumradius(multimesh)

# Parameters
alpha = 400.0

def tensor_jump(v, n):
    return outer(v('+'), n('+')) + outer(v('-'), n('-'))

def a_h(v, w):
    return inner(grad(v), grad(w))*dX \
         - inner(avg(grad(v)), tensor_jump(w, n))*dI \
         - inner(avg(grad(w)), tensor_jump(v, n))*dI \
         + alpha/avg(h) * inner(jump(v), jump(w))*dI

def b_h(v, q):
    return -div(v)*q*dX + jump(v, n)*avg(q)*dI

def l_h(v, q, f):
    return inner(f, v)*dX

def s_O(v, w):
    return inner(jump(grad(v)), jump(grad(w)))*dO

def s_C(v, q, w, r):
    return h*h*inner(-div(grad(v)) + grad(q), -div(grad(w)) - grad(r))*dC

def l_C(v, q, f):
    return h*h*inner(f, -div(grad(v)) - grad(q))*dC

# Define bilinear form
a = a_h(u, v) + b_h(v, p) + b_h(u, q) + s_O(u, v) + s_C(u, p, v, q)

# Define linear form
L  = l_h(v, q, f) + l_C(v, q, f)

# Assemble linear system
A = assemble_multimesh(a)
b = assemble_multimesh(L)

# Create boundary values
in_exp = "sin((x[1] - ymin)*DOLFIN_PI/yd)*sin((x[2] - zmin)*DOLFIN_PI/zd)"
inflow_value = Expression((in_exp, "0.0", "0.0"), \
			 ymin = ymin, zmin = zmin, yd = yd, zd = zd)
outflow_value = Constant(0)
noslip_value = Constant((0, 0, 0))

# Create subdomains for boundary conditions
inflow_boundary = InflowBoundary()
outflow_boundary = OutflowBoundary()
noslip_boundary = NoslipBoundary()

# Create subspaces for boundary conditions
V = MultiMeshSubSpace(W, 0)
Q = MultiMeshSubSpace(W, 1)

# Create boundary conditions
bc0 = MultiMeshDirichletBC(V, noslip_value,  noslip_boundary)
bc1 = MultiMeshDirichletBC(V, inflow_value,  inflow_boundary)
bc2 = MultiMeshDirichletBC(Q, outflow_value, outflow_boundary)

# Apply boundary conditions
bc0.apply(A, b)
bc1.apply(A, b)
bc2.apply(A, b)

# Create subdomains for sink domains
sink_ground_tree = BoundingBoxTree()
sink_ground_tree.build(sinkmesh_ground)
sink_ground = SinkGround()  

# Create sink domains
sd_ground = MultiMeshDirichletBC(V, noslip_value, sink_ground)

# Apply sink domains 
sd_ground.apply(A, b)

# Compute solution
w = MultiMeshFunction(W)
solve(A, w.vector(), b)

# FIXME: w.part(i).split() not working for extracted parts
# FIXME: since they are only dolfin::Functions

# Extract solution components
u_air = w.part(0).sub(0)
u_hb = w.part(1).sub(0)
u_sg = w.part(2).sub(0)
p_air = w.part(0).sub(1)
p_hb = w.part(1).sub(1)
p_sg = w.part(2).sub(1)

# Save to file
File("solutions_sinkmesh/u_air.pvd") << u_air
File("solutions_sinkmesh/u_hb.pvd") << u_hb
File("solutions_sinkmesh/u_sg.pvd") << u_sg
File("solutions_sinkmesh/p_air.pvd") << p_air
File("solutions_sinkmesh/p_hb.pvd") << p_hb
File("solutions_sinkmesh/p_sg.pvd") << p_sg

# Plot solution
# plot(W.multimesh())
#plot(mesh_0)
plot(u_air, title="u_air", interactive=True)
plot(u_hb, title="u_hb", interactive=True)
#plot(p0, title="p_0")
#plot(p1, title="p_1")
#interactive()
