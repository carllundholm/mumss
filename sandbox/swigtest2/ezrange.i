%module ezrange

%{
    #define SWIG_FILE_WITH_INIT
    #include "ezrange.h"
%}

%include "numpy.i"

%init %{
    import_array();
%}

%apply (int* ARGOUT_ARRAY1, int DIM1) {(int* rangevec, int n)}
%apply (double* IN_ARRAY1, int DIM1) {(double* in_array, int size_in)}
%apply (double* INPLACE_ARRAY1, int DIM1) {(double* out_array, int size_out)}

%include "ezrange.h"
