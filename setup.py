from distutils.core import setup, Extension
import numpy, os

os.environ['CC'] = 'g++'
module_rastering = Extension('mumss._rastering',
                             sources=['ext/rastering.cpp', 'ext/rastering.i'],
                             include_dirs=[numpy.get_include()])

setup(name='MUMSS',
      version=0.0,
      packages=['mumss'],
      ext_modules=[module_rastering])
